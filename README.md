# Git Practice Task

This is a simple Hello World Program in C++

## How to Run Code

- First go to blue **Clone** option on top right side and copy the URL **Clone with HTTP** or the URL **Clone with SSH**
- Here open your Linux `Terminal` and select a `Directory` where you want to work and run following command `git clone < URL >`
- Upon successful clone of repo set working directory to **git-practice-task** using `cd`

### Compile Hello World.cpp file

- Compile main.cpp using the **GCC** compiler using command `g++ -o out Hello\ World.cpp`
- To view output run command `./out`

#### Added Feature to Enter and display Name

*Note:( This feature was implimented later as part of the task )*  

- On prompt enter your Name
- It will display your Name

